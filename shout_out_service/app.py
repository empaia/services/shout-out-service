import asyncio
from contextlib import asynccontextmanager

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api.v1.add_routes import add_routes_v1
from .daemon_tick import make_daemon_tick
from .late_init import LateInit
from .singletons import logger, settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""


late_init = LateInit()


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    while late_init.pool is None:
        try:
            late_init.pool = await asyncpg.create_pool(
                user=settings.db_username,
                password=settings.db_password,
                database=settings.db,
                host=settings.db_host,
                port=settings.db_port,
            )
        except (ConnectionRefusedError, asyncpg.exceptions.CannotConnectNowError):
            logger.info("Waiting for DB...")
            await asyncio.sleep(1)

    logger.info("DB connection established.")

    late_init.daemon_tick = make_daemon_tick(address=settings.daemon_zmq_address, logger=logger)
    yield


app = FastAPI(
    title="Shout Out Service",
    version=version,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
    lifespan=lifespan,
)

if settings.cors_allow_origins:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=settings.cors_allow_credentials,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.get("/alive", tags=["Private"])
async def _():
    return {"status": "ok", "version": version}


app_v1 = FastAPI(openapi_url=openapi_url)
add_routes_v1(app_v1, late_init)
app.mount("/v1", app_v1)
