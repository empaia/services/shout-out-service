from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db
from .migrate_db import TABLE_NAME_PREFIX

TABLES = [f"{TABLE_NAME_PREFIX}_migration_steps", "messages", "shout_outs"]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table};")
        except UndefinedTableError as e:
            print(e)
