import asyncio
import logging
import sys

import typer

from ..access_token_tools import AccessTokenTools
from ..settings import Settings
from .drop_db import run_drop_db
from .migrate_db import run_migrate_db

logger = logging.getLogger("sosctl")
logger.addHandler(logging.StreamHandler(sys.stdout))

app = typer.Typer()


@app.command()
def migrate_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_migrate_db())


@app.command()
def drop_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_drop_db())


@app.command()
def create_access_token_tool_keys():
    # The constructor creates the public/private keys, if they do not exist:
    settings = Settings()
    keys_creator = AccessTokenTools(settings.rsa_keys_directory)
    keys_creator.create_rsa_key_files(logger)


def main():
    app()
