from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    sql = """
    CREATE TABLE shout_outs (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        recipient_id TEXT NOT NULL,
        timeout_interval INTEGER NOT NULL,
        status TEXT NOT NULL,
        created_at timestamp NOT NULL,
        updated_at timestamp NOT NULL,
        anonymized BOOLEAN NOT NULL,
        confirmed_by_message_id uuid,
        error_message TEXT
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE messages (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        shout_out_id uuid NOT NULL,
        sink TEXT NOT NULL,
        updated_at timestamp NOT NULL,
        anonymized BOOLEAN NOT NULL,
        status TEXT NOT NULL,
        error_message TEXT,
        _to TEXT,
        subject TEXT,
        content TEXT,
        content_html TEXT,
        CONSTRAINT fk_shout_out FOREIGN KEY(shout_out_id) REFERENCES shout_outs(id)
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE INDEX shout_outs_recipient_id ON shout_outs USING btree (recipient_id);
    CREATE INDEX shout_outs_status ON shout_outs USING btree (status);
    CREATE INDEX shout_outs_created_at ON shout_outs USING btree (created_at DESC);
    CREATE INDEX shout_outs_anonymized ON shout_outs USING btree (anonymized);
    CREATE INDEX messages_shout_out_id ON messages USING btree (shout_out_id);
    CREATE INDEX messages_status ON messages USING btree (status);
    CREATE INDEX messages_anonymized ON messages USING btree (anonymized);
    """
    await conn.execute(sql)
