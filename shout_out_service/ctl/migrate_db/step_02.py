async def step_02(conn):
    sql = """
    ALTER TABLE messages
    ADD COLUMN routing_key TEXT;
    ALTER TABLE messages
    ADD COLUMN sort_key INTEGER NOT NULL;
    """
    await conn.execute(sql)
