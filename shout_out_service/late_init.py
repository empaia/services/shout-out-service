from asyncpg import Pool


class AlreadyInitializedError(Exception):
    pass


class LateInit:
    def __init__(self):
        self._pool = None
        self._daemon_tick = None

    @property
    def pool(self):
        return self._pool

    @pool.setter
    def pool(self, pool: Pool):
        if self._pool is not None:
            raise AlreadyInitializedError("Pool already initialized.")

        self._pool = pool

    @property
    def daemon_tick(self):
        return self._daemon_tick

    @daemon_tick.setter
    def daemon_tick(self, daemon_tick):
        if self._daemon_tick is not None:
            raise AlreadyInitializedError("Daemon Tick already initialized.")

        self._daemon_tick = daemon_tick
