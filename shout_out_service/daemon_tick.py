import zmq


def make_daemon_tick(address, logger):
    context: zmq.Context = zmq.Context()
    socket: zmq.Socket = context.socket(zmq.PUSH)
    socket.connect(address)

    def daemon_tick():
        try:
            socket.send(data=b"tick", flags=zmq.NOBLOCK)
            logger.debug("Tick!")
        except zmq.ZMQError:
            logger.error("ZMQ Socket is full!")

    return daemon_tick
