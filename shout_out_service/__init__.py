from importlib.metadata import version

__version__ = version("shout-out-service")
