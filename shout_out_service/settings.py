from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    disable_openapi: bool = False
    root_path: str = ""
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    api_v1_integration: str = ""
    debug: bool = False
    db_host: str = "localhost"
    db_port: str = "5432"
    db_username: str = "empaia"
    db_password: str = "secret"
    db: str = "sos"  # change default db name, ts is Template Service
    rsa_keys_directory: str = "./rsa/"
    daemon_poll_interval: int = 15  # seconds
    daemon_zmq_port: int = 5556
    daemon_zmq_address: str = "tcp://sosd:5556"
    enable_sink_smtp: bool = False
    smtp_message_from: str = "appuser@smtpd"
    smtp_host: str = "localhost"
    smtp_port: int = 1025
    smtp_username: str = ""
    smtp_password: str = ""
    smtp_timeout: int = 60
    smtp_use_tls: bool = False
    smtp_start_tls: bool = False
    smtp_validate_certs: bool = True
    enable_sink_amqp: bool = False
    amqp_host: str = "localhost"
    amqp_port: int = 5672
    amqp_username: str = "guest"
    amqp_password: str = "guest"
    amqp_virtualhost: str = "/"
    amqp_ssl: bool = False

    model_config = SettingsConfigDict(env_file=".env", env_prefix="SOS_", extra="ignore")
