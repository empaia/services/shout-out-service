import json

import aio_pika
from fastapi import HTTPException, status

from ...models import PutDirectAMQP
from ...singletons import api_v1_integration, pika_pool, settings


def add_routes_direct(app, late_init):
    @app.put(
        "/direct/amqp",
        tags=["Direct"],
        responses={
            200: {"description": "Direct AMQP access successful."},
            428: {"description": "AMQP sink must be enabled in server settings."},
        },
    )
    async def _(
        put_direct_amqp: PutDirectAMQP,
        _=api_v1_integration.global_depends(),
    ) -> None:
        if not settings.enable_sink_amqp:
            raise HTTPException(
                status_code=status.HTTP_428_PRECONDITION_REQUIRED,
                detail="AMQP sink must be enabled in server settings.",
            )

        message_body_amqp_encoded = json.dumps(put_direct_amqp.data).encode("utf-8")

        async with pika_pool.acquire() as connection:
            async with connection.channel() as channel:
                await channel.default_exchange.publish(
                    aio_pika.Message(body=message_body_amqp_encoded),
                    routing_key=put_direct_amqp.routing_key,
                )
