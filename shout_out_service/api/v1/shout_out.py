from fastapi import Body, Query
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ...db import db_clients
from ...models import ItemCount, PostShoutOut, PutConfirm, PutError, ShoutOut, ShoutOutList, ShoutOutQuery
from ...singletons import access_token_tools, api_v1_integration


def add_routes_shout_out(app, late_init):
    @app.post(
        "/shout-outs",
        response_model=ShoutOut,
        tags=["Shout Outs"],
    )
    async def _(
        post_shout_out: PostShoutOut,
        _=api_v1_integration.global_depends(),
    ) -> ShoutOut:
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client
            shout_out = await so_client.add(post_shout_out=post_shout_out)

            late_init.daemon_tick()
            return shout_out

    @app.put(
        "/shout-outs/query",
        response_model=ShoutOutList,
        responses={
            200: {"description": "List of matching items, including total number available"},
        },
    )
    async def _(
        query: ShoutOutQuery = Body(..., description="Query for filtering the returned items"),
        skip: ItemCount = Query(None, description="Number of items to skip, for paging"),
        limit: ItemCount = Query(None, description="Number of items to return, for paging"),
        _=api_v1_integration.global_depends(),
    ) -> ShoutOutList:
        """Get list of filtered ShoutOut, with paging."""
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client
            shout_outs = await so_client.get_all(query=query, skip=skip, limit=limit)
            if shout_outs is None:
                raise HTTPException(status_code=400, detail="DataError")
            return shout_outs

    @app.put(
        "/shout-outs/confirm",
        response_model=ShoutOut,
        tags=["Shout Outs"],
    )
    async def _(
        put_confirm: PutConfirm,
        _=api_v1_integration.global_depends(),
    ) -> ShoutOut:
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client

            message_id = str(put_confirm.message_id)

            query = ShoutOutQuery(messages=[message_id])
            shout_outs = await so_client.get_all(query=query)
            if not shout_outs.item_count == 1:
                raise HTTPException(status_code=404, detail=f"Shout Out for message with ID {message_id} not found.")

            shout_out_id = shout_outs.items[0].id
            shout_out = await so_client.update_confirm(shout_out_id=shout_out_id, put_confirm=put_confirm)
            if shout_out is None:
                raise HTTPException(status_code=400, detail=f"Shout Out with ID {shout_out_id} could not be confirmed.")

            late_init.daemon_tick()
            return shout_out

    @app.get(
        "/shout-outs/{shout_out_id}",
        response_model=ShoutOut,
        tags=["Shout Outs"],
    )
    async def _(
        shout_out_id: UUID4,
        _=api_v1_integration.global_depends(),
    ) -> ShoutOut:
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client
            shout_out = await so_client.get(shout_out_id=shout_out_id)
            if shout_out is None:
                raise HTTPException(status_code=404, detail=f"Shout Out with id {shout_out_id} not found.")
            return shout_out

    @app.put(
        "/shout-outs/{shout_out_id}/error",
        response_model=ShoutOut,
        tags=["Shout Outs"],
    )
    async def _(
        shout_out_id: UUID4,
        put_error: PutError,
        _=api_v1_integration.global_depends(),
    ) -> ShoutOut:
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client
            shout_out = await so_client.get(shout_out_id=shout_out_id)
            if shout_out is None:
                raise HTTPException(status_code=404, detail=f"Shout Out with id {shout_out_id} not found.")

            shout_out = await so_client.update_error(shout_out_id=shout_out_id, put_error=put_error)
            if shout_out is None:
                raise HTTPException(status_code=400, detail="Shout Out error could not be set.")

            late_init.daemon_tick()
            return shout_out

    @app.get(
        "/public-key",
        response_model=bytes,
        responses={
            200: {"description": "Public key for validating Access Tokens created for Scopes"},
        },
        tags=["Public"],
    )
    async def _() -> bytes:
        """Get the public key that can be used for validating the Access Tokens created for the Scopes."""
        return access_token_tools.public_key
