from .direct import add_routes_direct
from .shout_out import add_routes_shout_out


def add_routes_v1(app, late_init):
    add_routes_shout_out(app, late_init)
    add_routes_direct(app, late_init)
