from enum import Enum
from typing import List, Literal, Optional, Union

from pydantic import UUID4, BaseModel, ConfigDict, Field, conint, conlist, constr

Id = constr(strip_whitespace=True, min_length=1, max_length=50)
MessageTo = constr(strip_whitespace=True, min_length=1)
ErrorMessage = constr(strip_whitespace=True, min_length=1)
Timestamp = conint(ge=0)
Interval = conint(gt=0)
ItemCount = conint(ge=0)


class RestrictedBaseModel(BaseModel):
    """Abstract Super-class not allowing unknown fields in the **kwargs."""

    model_config = ConfigDict(extra="forbid")


class ShoutOutStatus(str, Enum):
    UNCONFIRMED = "UNCONFIRMED"
    CONFIRMED = "CONFIRMED"  # final state
    ERROR = "ERROR"  # final state
    TIMEOUT = "TIMEOUT"  # final state


SHOUT_OUT_FINAL_STATES = [
    ShoutOutStatus.CONFIRMED,
    ShoutOutStatus.ERROR,
    ShoutOutStatus.TIMEOUT,
]


class MessageStatus(str, Enum):
    READY = "READY"
    PROCESSING = "PROCESSING"
    SENT = "SENT"  # final state
    ERROR = "ERROR"  # final state


class Sink(str, Enum):
    SMTP = "SMTP"
    AMQP = "AMQP"


MESSAGE_FINAL_STATES = [
    MessageStatus.SENT,
    MessageStatus.ERROR,
]


class BaseMessage(RestrictedBaseModel):
    id: UUID4
    status: MessageStatus
    updated_at: int
    anonymized: bool
    error_message: Optional[str] = None


class PostMessageSMTP(RestrictedBaseModel):
    sink: Literal[Sink.SMTP]
    to: MessageTo
    subject: str
    content: str
    content_html: Optional[str] = None


class PostMessageAMQP(RestrictedBaseModel):
    sink: Literal[Sink.AMQP]
    content: str
    routing_key: Optional[str] = None  # default is recipient-id


class MessageSMTP(BaseMessage, PostMessageSMTP):
    pass


class MessageAMQP(BaseMessage, PostMessageAMQP):
    pass


PostMessage = Union[PostMessageSMTP, PostMessageAMQP]
Message = Union[MessageSMTP, MessageAMQP]


class MessageParser(RestrictedBaseModel):
    message: Message


class PostShoutOut(RestrictedBaseModel):
    recipient_id: Id
    timeout_interval: Interval  # seconds
    messages: List[PostMessage]


class ShoutOut(PostShoutOut):
    id: UUID4
    status: ShoutOutStatus
    created_at: int
    updated_at: int
    anonymized: bool
    messages: List[Message]
    confirmed_by_message_id: Optional[UUID4] = None
    error_message: Optional[ErrorMessage] = None

    __hash__ = object.__hash__


class ShoutOutQuery(RestrictedBaseModel):
    """
    Query for ShoutOuts that have to match _any_ of the values in _all_ the provided fields.
    """

    statuses: Optional[conlist(ShoutOutStatus, min_length=1)] = Field(
        default=None,
        examples=[[ShoutOutStatus.UNCONFIRMED.value, ShoutOutStatus.CONFIRMED.value]],
        description="List of ShoutOut Status values",
    )

    recipients: Optional[conlist(Id, min_length=1)] = Field(
        default=None,
        examples=[["32d923c1-96fa-45f6-99c3-8747269679ba"]],
        description="List of ShoutOut recipient IDs",
    )

    messages: Optional[conlist(Id, min_length=1)] = Field(
        default=None,
        examples=[["32d923c1-96fa-45f6-99c3-8747269679ba"]],
        description="List of ShoutOut message IDs",
    )


class ShoutOutList(RestrictedBaseModel):
    """ShoutOut query result."""

    item_count: ItemCount = Field(
        examples=[1],
        description="Number of ShoutOuts as queried without skip and limit applied.",
    )

    items: List[ShoutOut] = Field(
        examples=[
            [
                ShoutOut(
                    id="a10648a7-340d-43fc-a2d9-4d91cc86f33f",
                    recipient_id="32d923c1-96fa-45f6-99c3-8747269679ba",
                    timeout_interval=60 * 60 * 24,  # 1d
                    anonymized=False,
                    messages=[],
                    status=ShoutOutStatus.UNCONFIRMED,
                    created_at=1623349180,
                    updated_at=1623349180,
                )
            ]
        ],
        description="List of ShoutOut items as queried with skip and limit applied.",
    )


class PutError(RestrictedBaseModel):
    error_message: ErrorMessage


class PutConfirm(RestrictedBaseModel):
    message_id: UUID4


class MessageBodyAMQP(RestrictedBaseModel):
    content: str
    message_id: str
    shout_out_token: str


class PutDirectAMQP(RestrictedBaseModel):
    data: dict
    routing_key: str
