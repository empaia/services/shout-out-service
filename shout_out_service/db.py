import json

from asyncpg import Connection

from .shout_out_client import ShoutOutClient


def _jsonb_dumps(value):
    return b"\x01" + json.dumps(value).encode("utf-8")


def _jsonb_loads(value: bytes):
    return json.loads(value[1:].decode("utf-8"))


async def set_type_codecs(conn: Connection):
    await conn.set_type_codec("json", encoder=json.dumps, decoder=json.loads, schema="pg_catalog", format="text")
    await conn.set_type_codec("jsonb", encoder=_jsonb_dumps, decoder=_jsonb_loads, schema="pg_catalog", format="binary")


class Clients:
    def __init__(self, shout_out_client):
        self.shout_out_client = shout_out_client


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    shout_out_client = ShoutOutClient(conn=conn)
    return Clients(shout_out_client=shout_out_client)
