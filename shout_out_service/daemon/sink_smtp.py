import asyncio
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import List

import aiosmtplib

from ..db import db_clients
from ..models import MessageSMTP, MessageStatus, ShoutOut, Sink
from ..singletons import access_token_tools, settings
from .helpers import determine_token_exp
from .singletons import ANONYMIZED_TEXT, TOKEN_PLACEHOLDER_TEXT, logger


class SinkSMTP:
    def __init__(self, pool):
        self._pool = pool

    async def process(self, unconfirmed_shout_outs: List[ShoutOut], unanonymized_shout_outs: List[ShoutOut]):
        tasks = []

        for shout_out in unconfirmed_shout_outs:
            for message in shout_out.messages:
                if message.sink != Sink.SMTP:
                    continue
                if message.status == MessageStatus.READY:
                    tasks.append(
                        self._send(
                            recipient_id=shout_out.recipient_id,
                            timeout_interval=shout_out.timeout_interval,
                            created_at=shout_out.created_at,
                            message=message,
                        )
                    )

        for shout_out in unanonymized_shout_outs:
            for message in shout_out.messages:
                if message.sink != Sink.SMTP:
                    continue
                if not message.anonymized:
                    tasks.append(self._anonymize(message=message))

        asyncio.gather(*tasks)

    async def _send(self, recipient_id, timeout_interval, created_at, message: MessageSMTP):
        async with self._pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client

            updated_message = await so_client.update_message_status(
                message_id=message.id,
                status=MessageStatus.PROCESSING,
                where_status=MessageStatus.READY,
            )

            if updated_message is None:
                logger.error("Could not update status %s for message %s", MessageStatus.PROCESSING.value, message.id)
                return

            logger.info("Updated status %s for message %s", MessageStatus.PROCESSING.value, message.id)

            try:
                content = message.content
                content_html = message.content_html

                if TOKEN_PLACEHOLDER_TEXT in content:
                    current_timestamp = await so_client.current_timestamp()
                    exp = determine_token_exp(
                        timeout_interval=timeout_interval, created_at=created_at, current_timestamp=current_timestamp
                    )
                    shout_out_token = access_token_tools.create_token(
                        exp=exp,
                        recipient_id=recipient_id,
                        message_id=message.id,
                        sink=message.sink,
                    )
                    content = content.replace(TOKEN_PLACEHOLDER_TEXT, shout_out_token)

                    if content_html is not None:
                        content_html = content_html.replace(TOKEN_PLACEHOLDER_TEXT, shout_out_token)

                email_message = EmailMessage()
                email_message.set_content(content)

                if content_html is not None:
                    email_message = MIMEMultipart("alternative")
                    email_message.attach(MIMEText(content, "plain", "utf-8"))
                    email_message.attach(MIMEText(content_html, "html", "utf-8"))

                email_message["From"] = settings.smtp_message_from
                email_message["To"] = message.to
                email_message["Subject"] = message.subject

                smtp_username = None
                if settings.smtp_username != "":
                    smtp_username = settings.smtp_username

                smtp_password = None
                if settings.smtp_password != "":
                    smtp_password = settings.smtp_password

                await aiosmtplib.send(
                    email_message,
                    hostname=settings.smtp_host,
                    port=settings.smtp_port,
                    username=smtp_username,
                    password=smtp_password,
                    timeout=settings.smtp_timeout,
                    use_tls=settings.smtp_use_tls,
                    start_tls=settings.smtp_start_tls,
                    validate_certs=settings.smtp_validate_certs,
                )

                updated_message = await so_client.update_message_status(
                    message_id=message.id,
                    status=MessageStatus.SENT,
                    where_status=MessageStatus.PROCESSING,
                )

                if updated_message is None:
                    raise Exception(f"Could not update status {MessageStatus.SENT.value}.")

            except Exception as e:
                logger.error("Could not send SMTP message %s: %s", message.id, e)

                updated_message = await so_client.update_message_status(
                    message_id=message.id,
                    status=MessageStatus.ERROR,
                    where_status=MessageStatus.PROCESSING,
                    error_message=str(e),
                )

                if updated_message is None:
                    logger.error("Could not update status %s for message %s", MessageStatus.ERROR.value, message.id)

    async def _anonymize(self, message: MessageSMTP):
        async with self._pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client

            anonymized_data = {
                "to": ANONYMIZED_TEXT,
                "subject": ANONYMIZED_TEXT,
                "content": ANONYMIZED_TEXT,
            }

            if message.content_html:
                anonymized_data["content_html"] = ANONYMIZED_TEXT

            updated_message = await so_client.update_message_anonymize(message_id=message.id, **anonymized_data)
            if updated_message is None:
                logger.error("Could not anonymize message %s", message.id)
