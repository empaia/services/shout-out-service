import asyncio
from typing import List

import asyncpg
import zmq
import zmq.asyncio

from ..daemon_tick import make_daemon_tick
from ..db import db_clients
from ..models import SHOUT_OUT_FINAL_STATES, ShoutOut, ShoutOutList, ShoutOutQuery, ShoutOutStatus
from ..shout_out_client import ShoutOutClient
from ..singletons import settings
from .singletons import logger
from .sink_amqp import SinkAMQP
from .sink_smtp import SinkSMTP


async def _update_timeout(unconfirmed_shout_outs: List[ShoutOut], so_client: ShoutOutClient):
    current_timestamp = await so_client.current_timestamp()
    remaining_shout_outs = []

    for shout_out in unconfirmed_shout_outs:
        timeout_timestamp = shout_out.created_at + shout_out.timeout_interval

        if timeout_timestamp > current_timestamp:
            remaining_shout_outs.append(shout_out)
            continue

        logger.info("Shout Out %s timeout.", shout_out.id)

        updated_shout_out = await so_client.update_timeout(shout_out.id)
        if updated_shout_out is None:
            logger.error("Could not set timeout for Shout Out %s", shout_out.id)

    return remaining_shout_outs


async def _update_anonymize(unanonymized_shout_outs: List[ShoutOut], so_client: ShoutOutClient):
    remaining_shout_outs = []

    for shout_out in unanonymized_shout_outs:
        has_unanonymized_messages = False
        for message in shout_out.messages:
            if not message.anonymized:
                has_unanonymized_messages = True
                break

        if has_unanonymized_messages:
            remaining_shout_outs.append(shout_out)
            continue

        logger.info("Shout Out %s messages are anonmyized.", shout_out.id)

        updated_shout_out_id = await so_client.update_anonymize(shout_out.id)
        if updated_shout_out_id is None:
            logger.error("Could not set anonymized for Shout Out %s", shout_out.id)

    return remaining_shout_outs


async def run_async():
    context: zmq.asyncio.Context = zmq.asyncio.Context()
    socket: zmq.Socket = context.socket(zmq.PULL)
    socket.setsockopt(zmq.CONFLATE, 1)  # only last message is relevant
    socket.bind(f"tcp://*:{settings.daemon_zmq_port}")

    pool = None
    while pool is None:
        try:
            pool = await asyncpg.create_pool(
                user=settings.db_username,
                password=settings.db_password,
                database=settings.db,
                host=settings.db_host,
                port=settings.db_port,
            )
        except (ConnectionRefusedError, asyncpg.exceptions.CannotConnectNowError):
            logger.info("Waiting for DB...")
            await asyncio.sleep(1)

    unconfirmed_query = ShoutOutQuery(statuses=[ShoutOutStatus.UNCONFIRMED.value])
    final_query = ShoutOutQuery(statuses=[s.value for s in SHOUT_OUT_FINAL_STATES])

    sinks = []
    if settings.enable_sink_smtp:
        sinks.append(SinkSMTP(pool=pool))
    if settings.enable_sink_amqp:
        sinks.append(SinkAMQP(pool=pool))

    while True:
        await socket.recv()
        logger.debug("Tock!")

        async with pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client

            shout_out_list: ShoutOutList = await so_client.get_all(query=unconfirmed_query)
            unconfirmed_shout_outs = shout_out_list.items
            unconfirmed_shout_outs = await _update_timeout(
                unconfirmed_shout_outs=unconfirmed_shout_outs, so_client=so_client
            )

            shout_out_list: ShoutOutList = await so_client.get_all(query=final_query, anonymized=False)
            unanonymized_shout_outs = shout_out_list.items
            unanonymized_shout_outs = await _update_anonymize(
                unanonymized_shout_outs=unanonymized_shout_outs, so_client=so_client
            )

        tasks = []
        for sink in sinks:
            task = sink.process(
                unconfirmed_shout_outs=unconfirmed_shout_outs,
                unanonymized_shout_outs=unanonymized_shout_outs,
            )
            tasks.append(task)
            task = sink

        asyncio.gather(*tasks)


async def run_trigger():
    daemon_tick = make_daemon_tick(address=f"tcp://localhost:{settings.daemon_zmq_port}", logger=logger)

    while True:
        daemon_tick()
        logger.debug("Sleep %d seconds...", settings.daemon_poll_interval)
        await asyncio.sleep(settings.daemon_poll_interval)


def run():
    loop = asyncio.get_event_loop()
    loop.create_task(run_trigger())
    loop.run_until_complete(run_async())
