import asyncio
from typing import List

import aio_pika

from ..db import db_clients
from ..models import MessageAMQP, MessageBodyAMQP, MessageStatus, ShoutOut, Sink
from ..singletons import access_token_tools, pika_pool
from .helpers import determine_token_exp
from .singletons import ANONYMIZED_TEXT, logger


class SinkAMQP:
    def __init__(self, pool):
        self._pool = pool

    async def process(self, unconfirmed_shout_outs: List[ShoutOut], unanonymized_shout_outs: List[ShoutOut]):
        tasks = []

        for shout_out in unconfirmed_shout_outs:
            for message in shout_out.messages:
                if message.sink != Sink.AMQP:
                    continue
                if message.status == MessageStatus.READY:
                    tasks.append(
                        self._send(
                            recipient_id=shout_out.recipient_id,
                            timeout_interval=shout_out.timeout_interval,
                            created_at=shout_out.created_at,
                            message=message,
                        )
                    )

        for shout_out in unanonymized_shout_outs:
            for message in shout_out.messages:
                if message.sink != Sink.AMQP:
                    continue
                if not message.anonymized:
                    tasks.append(self._anonymize(message=message))

        asyncio.gather(*tasks)

    async def _send(self, recipient_id, timeout_interval, created_at, message: MessageAMQP):
        async with self._pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client

            updated_message = await so_client.update_message_status(
                message_id=message.id,
                status=MessageStatus.PROCESSING,
                where_status=MessageStatus.READY,
            )

            if updated_message is None:
                logger.error("Could not update status %s for message %s", MessageStatus.PROCESSING.value, message.id)
                return

            logger.info("Updated status %s for message %s", MessageStatus.PROCESSING.value, message.id)

            try:
                current_timestamp = await so_client.current_timestamp()
                exp = determine_token_exp(
                    timeout_interval=timeout_interval, created_at=created_at, current_timestamp=current_timestamp
                )
                shout_out_token = access_token_tools.create_token(
                    exp=exp,
                    recipient_id=recipient_id,
                    message_id=message.id,
                    sink=message.sink,
                )
                message_body_amqp = MessageBodyAMQP(
                    content=message.content,
                    message_id=str(message.id),
                    shout_out_token=shout_out_token,
                )
                message_body_amqp_encoded = message_body_amqp.model_dump_json().encode("utf-8")

                async with pika_pool.acquire() as connection:
                    async with connection.channel() as channel:
                        routing_key = message.routing_key if message.routing_key is not None else recipient_id
                        await channel.default_exchange.publish(
                            aio_pika.Message(body=message_body_amqp_encoded),
                            routing_key=routing_key,
                        )

                updated_message = await so_client.update_message_status(
                    message_id=message.id,
                    status=MessageStatus.SENT,
                    where_status=MessageStatus.PROCESSING,
                )

                if updated_message is None:
                    raise Exception(f"Could not update status {MessageStatus.SENT.value}.")

            except Exception as e:
                logger.error("Could not send AMQP message %s: %s", message.id, e)

                updated_message = await so_client.update_message_status(
                    message_id=message.id,
                    status=MessageStatus.ERROR,
                    where_status=MessageStatus.PROCESSING,
                    error_message=str(e),
                )

                if updated_message is None:
                    logger.error("Could not update status %s for message %s", MessageStatus.ERROR.value, message.id)

    async def _anonymize(self, message: MessageAMQP):
        async with self._pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            so_client = clients.shout_out_client

            anonymized_data = {"content": ANONYMIZED_TEXT}

            updated_message = await so_client.update_message_anonymize(message_id=message.id, **anonymized_data)
            if updated_message is None:
                logger.error("Could not anonymize message %s", message.id)
