import logging

from ..singletons import settings

TOKEN_PLACEHOLDER_TEXT = "{{SHOUT_OUT_TOKEN}}"
ANONYMIZED_TEXT = "ANONYMIZED"


logging.basicConfig()
if settings.debug:
    logging.root.setLevel(logging.DEBUG)
else:
    logging.root.setLevel(logging.INFO)
logger = logging.getLogger("SOSD")
