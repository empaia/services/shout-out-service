def determine_token_exp(timeout_interval: int, created_at: int, current_timestamp: int):
    seconds_passed = current_timestamp - created_at
    seconds_remaining = timeout_interval - seconds_passed
    return current_timestamp + seconds_remaining
