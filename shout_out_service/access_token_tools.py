import logging
import os

import jwt
import rsa


class AccessTokenToolsException(RuntimeError):
    pass


class AccessTokenTools:
    def __init__(self, keys_directory: str):
        self._private_key = None
        self._public_key = None
        self.algorithm = "RS256"
        self.keys_directory = keys_directory

    @property
    def private_key(self):
        if self._private_key is None:
            self._private_key = self._read_key_file(rsa.PrivateKey)
        return self._private_key

    @property
    def public_key(self):
        if self._public_key is None:
            self._public_key = self._read_key_file(rsa.PublicKey)
        return self._public_key

    def create_rsa_key_files(self, logger: logging.Logger):
        if not os.path.isdir(self.keys_directory):
            logger.info(f"RSA keys directory does not exist... creating '{self.keys_directory}'")
            os.makedirs(self.keys_directory)
        self._ensure_key_files_exist(logger)

    def _get_key_file(self, keyType) -> str:
        return os.path.join(self.keys_directory, "rsa_id" if keyType is rsa.PrivateKey else "rsa_id.pub")

    def _ensure_key_files_exist(self, logger: logging.Logger):
        key_files = [
            self._get_key_file(rsa.PublicKey),
            self._get_key_file(rsa.PrivateKey),
        ]
        if not (os.path.isfile(key_files[0]) and os.path.isfile(key_files[1])):
            logger.debug(f"Creating RSA keys in directory '{self.keys_directory}': {key_files[0]}, {key_files[1]}")
            keys = rsa.newkeys(1024)
            for key_file, key in zip(key_files, keys):
                with open(key_file, "wb") as f:
                    f.write(key.save_pkcs1())

    def _read_key_file(self, keyType) -> bytes:
        if not os.path.isdir(self.keys_directory):
            raise AccessTokenToolsException(
                "Class AccessTokenTools requires existing RSA keys, "
                f"but the storage directory does not exist: {self.keys_directory}"
            )
        key_file = self._get_key_file(keyType)
        if not os.path.isfile(key_file):
            raise AccessTokenToolsException(
                "Class AccessTokenTools requires existing RSA keys, " f"but a key file does not exist: {key_file}"
            )
        with open(key_file, "rb") as f:
            key = keyType.load_pkcs1(f.read())
            return key.save_pkcs1()

    def create_token(self, exp, recipient_id, message_id, sink) -> str:
        payload = {
            "sub": str(recipient_id),
            "exp": exp,
            "message_id": str(message_id),
            "sink": sink,
        }
        return jwt.encode(payload, self.private_key, algorithm=self.algorithm)
