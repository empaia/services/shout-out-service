from typing import Optional

from asyncpg import Connection
from asyncpg.exceptions import DataError
from pydantic import UUID4

from .models import (
    SHOUT_OUT_FINAL_STATES,
    Message,
    MessageParser,
    MessageStatus,
    PostMessageAMQP,
    PostMessageSMTP,
    PostShoutOut,
    PutConfirm,
    PutError,
    ShoutOut,
    ShoutOutList,
    ShoutOutQuery,
    ShoutOutStatus,
)
from .singletons import logger

MESSAGE_TO_JSON = """json_strip_nulls(json_build_object(
    'id', id,
    'sink', sink,
    'status', status,
    'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
    'anonymized', anonymized,
    'error_message', error_message,
    'to', _to,
    'subject', subject,
    'content', content,
    'content_html', content_html,
    'routing_key', routing_key
))"""

SELECT_SHOUT_OUT_AND_MESSAGES = f"""
WITH shout_out_messages AS (
    SELECT *
    FROM messages
    WHERE messages.shout_out_id = $1
)
SELECT json_build_object(
    'id', id,
    'recipient_id', recipient_id,
    'timeout_interval', timeout_interval,
    'status', status,
    'created_at', EXTRACT(EPOCH FROM created_at)::int,
    'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
    'anonymized', anonymized,
    'messages', (
        SELECT coalesce(
            json_agg(
                {MESSAGE_TO_JSON}
                ORDER BY sort_key ASC
            ),
            '[]'::json
        )
        FROM shout_out_messages
    )
) shout_out
FROM shout_outs
WHERE id = $1;
"""


class ShoutOutClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    async def add(self, post_shout_out: PostShoutOut) -> ShoutOut:
        async with self.conn.transaction():
            sql = """
            INSERT INTO shout_outs (recipient_id, timeout_interval, status, created_at, updated_at, anonymized)
            VALUES ($1, $2, $3, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $4)
            RETURNING id, created_at;
            """
            logger.debug(sql)

            row = await self.conn.fetchrow(
                sql,
                post_shout_out.recipient_id,
                post_shout_out.timeout_interval,
                ShoutOutStatus.UNCONFIRMED.value,
                False,
            )
            shout_out_id = row["id"]
            created_at = row["created_at"]
            sort_key = 0

            for message in post_shout_out.messages:
                if isinstance(message, PostMessageSMTP):
                    sql = """
                    INSERT INTO messages (
                        shout_out_id,
                        sink,
                        status,
                        updated_at,
                        anonymized,
                        _to,
                        subject,
                        content,
                        content_html,
                        sort_key
                    )
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
                    RETURNING id
                    ;
                    """
                    logger.debug(sql)
                    _ = await self.conn.fetchrow(
                        sql,
                        shout_out_id,
                        message.sink,
                        MessageStatus.READY.value,
                        created_at,
                        False,
                        message.to,
                        message.subject,
                        message.content,
                        message.content_html,
                        sort_key,
                    )
                elif isinstance(message, PostMessageAMQP):
                    sql = """
                    INSERT INTO messages (
                        shout_out_id,
                        sink,
                        status,
                        updated_at,
                        anonymized,
                        content,
                        routing_key,
                        sort_key
                    )
                    VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                    RETURNING id
                    ;
                    """
                    logger.debug(sql)
                    _ = await self.conn.fetchrow(
                        sql,
                        shout_out_id,
                        message.sink,
                        MessageStatus.READY.value,
                        created_at,
                        False,
                        message.content,
                        message.routing_key,
                        sort_key,
                    )
                else:
                    raise Exception(f"Unknown Message Type: {type(message)}")

                sort_key += 1

            logger.debug(SELECT_SHOUT_OUT_AND_MESSAGES)
            row = await self.conn.fetchrow(SELECT_SHOUT_OUT_AND_MESSAGES, shout_out_id)

            return ShoutOut(**row["shout_out"])

    async def get(self, shout_out_id: UUID4) -> Optional[ShoutOut]:
        logger.debug(SELECT_SHOUT_OUT_AND_MESSAGES)
        row = await self.conn.fetchrow(SELECT_SHOUT_OUT_AND_MESSAGES, str(shout_out_id))

        if row is None:
            return

        return ShoutOut(**row["shout_out"])

    async def _update_status(
        self, shout_out_id: str, status: str, confirmed_by_message_id: str = None, error_message: str = None
    ) -> Optional[ShoutOut]:
        data = [shout_out_id, ShoutOutStatus.UNCONFIRMED.value, status]
        data_index = 4

        sql = """
        UPDATE shout_outs
        SET status=$3"""

        if confirmed_by_message_id is not None:
            sql += f""",
            confirmed_by_message_id=${data_index}"""
            data.append(confirmed_by_message_id)
            data_index += 1

        if error_message is not None:
            sql += f""",
            error_message=${data_index}"""
            data.append(error_message)
            data_index += 1

        sql += """,
            updated_at=CURRENT_TIMESTAMP
        WHERE id=$1 AND status=$2
        RETURNING id;
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, *data)
        except DataError as e:
            logger.info("DataError: %s", e)
            return None

        if row is None:
            return

        try:
            row = await self.conn.fetchrow(SELECT_SHOUT_OUT_AND_MESSAGES, shout_out_id)
        except DataError as e:
            logger.info("DataError: %s", e)
            return None

        return ShoutOut.model_validate(row["shout_out"])

    async def update_confirm(self, shout_out_id: UUID4, put_confirm: PutConfirm) -> Optional[ShoutOut]:
        return await self._update_status(
            shout_out_id=str(shout_out_id),
            status=ShoutOutStatus.CONFIRMED.value,
            confirmed_by_message_id=str(put_confirm.message_id),
        )

    async def update_timeout(self, shout_out_id: UUID4) -> Optional[ShoutOut]:
        return await self._update_status(
            shout_out_id=str(shout_out_id),
            status=ShoutOutStatus.TIMEOUT.value,
        )

    async def update_error(self, shout_out_id: UUID4, put_error: PutError) -> Optional[ShoutOut]:
        return await self._update_status(
            shout_out_id=str(shout_out_id),
            status=ShoutOutStatus.ERROR.value,
            error_message=str(put_error.error_message),
        )

    async def current_timestamp(self):
        sql = "SELECT EXTRACT(EPOCH FROM CURRENT_TIMESTAMP)::int;"
        row = await self.conn.fetchrow(sql)
        return row["extract"]

    async def update_anonymize(self, shout_out_id) -> Optional[UUID4]:
        data = [shout_out_id, [s.value for s in SHOUT_OUT_FINAL_STATES], False, True]
        sql = """
        UPDATE shout_outs
        SET anonymized=$4
        WHERE id=$1 AND status=ANY($2) AND anonymized=$3
        RETURNING id"""
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, *data)

        if row is None:
            return

        return row["id"]

    async def update_message_anonymize(
        self,
        message_id: UUID4,
        to: str = None,
        subject: str = None,
        content: str = None,
        content_html: str = None,
    ) -> Optional[Message]:
        anonymized_data = {
            "_to": to,
            "subject": subject,
            "content": content,
            "content_html": content_html,
        }

        data = [str(message_id), False, True]
        data_index = 4

        sql = """
        UPDATE messages
        SET anonymized=$3"""

        for key, val in anonymized_data.items():
            if val is None:
                continue

            sql += f""",
            {key}=${data_index}"""
            data.append(val)
            data_index += 1

        sql += f"""
        WHERE id=$1 AND anonymized=$2
        RETURNING {MESSAGE_TO_JSON};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, *data)

        if row is None:
            return

        message_parser = MessageParser(message=row["json_strip_nulls"])
        return message_parser.message

    async def update_message_status(
        self, message_id: UUID4, status: MessageStatus, where_status: MessageStatus = None, error_message: str = None
    ) -> Optional[Message]:
        data = [str(message_id), status.value]
        data_index = 3

        sql = """
        UPDATE messages
        SET status=$2,
            updated_at=CURRENT_TIMESTAMP"""

        if error_message is not None:
            sql += f""",
            error_message=${data_index}"""
            data.append(error_message)
            data_index += 1

        sql += """
        WHERE id=$1
        """

        if where_status is not None:
            sql += f"AND status=${data_index}"
            data.append(where_status.value)
            data_index += 1

        sql += f"""
        RETURNING {MESSAGE_TO_JSON};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, *data)

        if row is None:
            return

        message_parser = MessageParser(message=row["json_strip_nulls"])
        return message_parser.message

    @staticmethod
    def _query_transform(query_mapping: dict, start_index=1):
        transformed_query = ""
        query_terms = []
        query_data = []
        index = start_index

        for key, val in query_mapping.items():
            if val is None:
                continue

            if isinstance(val, list):
                query_terms.append(f"{key}=ANY(${index})")
            else:
                query_terms.append(f"{key}=${index}")

            query_data.append(val)
            index += 1

        if index > start_index:
            transformed_query = "WHERE " + " AND ".join(query_terms)
        return transformed_query, query_data, index

    async def get_all(
        self, query: ShoutOutQuery = None, anonymized=None, skip=None, limit=None
    ) -> Optional[ShoutOutList]:
        query_mapping = {"shout_outs.anonymized": anonymized}
        if query is not None:
            query_mapping["shout_outs.recipient_id"] = query.recipients
            query_mapping["shout_outs.status"] = query.statuses

            if query.messages is not None:
                sql = """
                SELECT json_agg(DISTINCT shout_out_id)
                FROM messages
                WHERE id = ANY($1);
                """
                logger.debug(sql)

                try:
                    row = await self.conn.fetchrow(sql, query.messages)
                except DataError as e:
                    logger.info("DataError: %s", e)
                    return None

                shout_out_ids = row["json_agg"]
                if shout_out_ids is None:
                    return ShoutOutList(item_count=0, items=[])

                query_mapping["shout_outs.id"] = shout_out_ids

        transformed_query, query_data, index = self._query_transform(query_mapping=query_mapping)

        # get item count
        sql = f"""
        SELECT COUNT(id)
        FROM shout_outs
        {transformed_query};
        """
        logger.debug(sql)

        try:
            row = await self.conn.fetchrow(sql, *query_data)
        except DataError as e:
            logger.info("DataError: %s", e)
            return None

        item_count = row["count"]

        # get items
        sub_sql = f"""SELECT *
            FROM shout_outs
            {transformed_query}
            ORDER BY created_at DESC"""

        if limit is not None:
            sub_sql += f"""
            LIMIT ${index}"""
            query_data.append(limit)
            index += 1

        if skip is not None:
            sub_sql += f"""
            OFFSET ${index}"""
            query_data.append(skip)
            index += 1

        sql = f"""
        WITH cte_messages AS (
            SELECT
                shout_out_id,
                json_agg(
                    {MESSAGE_TO_JSON}
                    ORDER BY sort_key ASC
                ) agg_messages
            FROM messages
            GROUP BY 1
        )
        SELECT
            json_agg(
                json_build_object(
                    'id', id,
                    'recipient_id', recipient_id,
                    'timeout_interval', timeout_interval,
                    'status', status,
                    'created_at', EXTRACT(EPOCH FROM created_at)::int,
                    'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
                    'anonymized', anonymized,
                    'messages', coalesce(agg_messages, '[]'::json)
                )
                ORDER BY created_at DESC
            ) agg_shout_outs
        FROM (
            {sub_sql}
        ) s
        LEFT JOIN cte_messages m
        ON m.shout_out_id = s.id;
        """
        logger.debug(sql)

        items = []
        if limit is None or limit > 0:
            try:
                row = await self.conn.fetchrow(sql, *query_data)
            except DataError as e:
                logger.info("DataError: %s", e)
                return None

            if row["agg_shout_outs"] is not None:
                items = row["agg_shout_outs"]

        return ShoutOutList(items=items, item_count=item_count)
