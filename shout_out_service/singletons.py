import logging

import aio_pika

from .access_token_tools import AccessTokenTools
from .api_integrations import get_api_v1_integration
from .settings import Settings

settings = Settings()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

api_v1_integration = get_api_v1_integration(settings, logger)

access_token_tools = AccessTokenTools(settings.rsa_keys_directory)


async def _get_connection():
    return await aio_pika.connect_robust(
        host=settings.amqp_host,
        port=settings.amqp_port,
        login=settings.amqp_username,
        password=settings.amqp_password,
        virtualhost=settings.amqp_virtualhost,
        ssl=settings.amqp_ssl,
    )


pika_pool = aio_pika.pool.Pool(_get_connection)
