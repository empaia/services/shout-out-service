FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.4.2@sha256:5c1a951baeed230dc36a9d36c3689faf7e639a3a2271eb4e64cc7660062d6f85 AS builder
COPY . /shout-out-service
WORKDIR /shout-out-service
RUN poetry build && poetry export --without-hashes -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.3.1@sha256:54c07b7d87e7d70248fa590f4ed7e0217e2e11b76bb569724807acc82488197b
COPY --from=builder /shout-out-service/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /shout-out-service/dist /artifacts
RUN pip install /artifacts/*.whl
COPY --chown=appuser:appuser ./run.sh /opt/app/bin/run.sh
COPY --chown=appuser:appuser ./rund.sh /opt/app/bin/rund.sh
WORKDIR /opt/app/bin/
RUN mkdir rsa
