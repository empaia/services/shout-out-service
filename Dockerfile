FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.4.3@sha256:ea0e74e3a795a00d0bf4a9cf3ed2fe308ae6db6dbb080be84077caeb57800251 AS builder
COPY . /shout-out-service
WORKDIR /shout-out-service
RUN poetry build && poetry export --without-hashes -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.3.2@sha256:978b81a5d1b6b2fcebbbab2888ce6c8fd41c231a0e9c1ed0f0de7d591981588f
COPY --from=builder /shout-out-service/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /shout-out-service/dist /artifacts
RUN pip install /artifacts/*.whl
COPY --chown=appuser:appuser ./run.sh /opt/app/bin/run.sh
COPY --chown=appuser:appuser ./rund.sh /opt/app/bin/rund.sh
WORKDIR /opt/app/bin/
RUN mkdir rsa
