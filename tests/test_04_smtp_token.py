from email.parser import Parser
from pprint import pprint
from time import sleep
from uuid import uuid4

import jwt
import requests

from shout_out_service.models import PostMessageSMTP, PostShoutOut, ShoutOut, ShoutOutStatus, Sink

from .singletons import mailhog_url, sos_url


def test_smtp_token_shout_out():
    random_subject = str(uuid4())
    random_recipient = str(uuid4())

    message_plain = PostMessageSMTP(
        sink="SMTP",
        to="To",
        subject=random_subject,
        content="Token: {{SHOUT_OUT_TOKEN}}\r\nToken: {{SHOUT_OUT_TOKEN}}",
    )
    post_shout_out = PostShoutOut(
        recipient_id=random_recipient,
        timeout_interval=30,
        messages=[message_plain],
    )
    r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.status == ShoutOutStatus.UNCONFIRMED

    sleep(10)

    r = requests.get(f"{mailhog_url}/api/v2/messages")
    r.raise_for_status()
    data = r.json()
    pprint(data)

    found_mail = None
    for mail in data["items"]:
        subject = mail["Content"]["Headers"]["Subject"][0]
        if subject == random_subject:
            found_mail = mail
            break

    assert found_mail is not None

    raw_data = found_mail["Raw"]["Data"]
    parser = Parser()
    message = parser.parsestr(raw_data)
    payload = message.get_payload()

    lines = payload.split("Token: ")
    normalized_lines = []
    for line in lines:
        if not line:
            continue
        normalized_lines.append(line.replace("=\r\n", "").strip("\r\n"))

    assert len(normalized_lines) == 2
    token_1 = normalized_lines[0]
    token_2 = normalized_lines[1]
    assert token_1 == token_2
    decoded_token = jwt.decode(token_1, options={"verify_signature": False})
    assert decoded_token["sub"] == random_recipient
    assert decoded_token["message_id"] == str(shout_out.messages[0].id)
    assert decoded_token["sink"] == Sink.SMTP
    exp_diff = decoded_token["exp"] - shout_out.created_at
    assert abs(exp_diff - shout_out.timeout_interval) == 0
