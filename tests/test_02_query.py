import uuid

import requests

from shout_out_service.models import PostMessageSMTP, PostShoutOut, ShoutOut, ShoutOutList, ShoutOutQuery, Sink

from .singletons import sos_url


def test_query():
    # insert data
    num_shout_outs = 3
    num_messages_per_shout_out = 2

    all_recipients = [str(uuid.uuid4()) for _ in range(num_shout_outs)]
    posted_shout_outs = []

    for i, recipient_id in enumerate(all_recipients):
        messages = []
        for j in range(num_messages_per_shout_out):
            message = PostMessageSMTP(
                sink=Sink.SMTP,
                to=f"To {i}, {j}",
                subject="Subject",
                content="Content",
            )
            messages.append(message)

        post_shout_out = PostShoutOut(recipient_id=recipient_id, timeout_interval=30, messages=messages)

        r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
        r.raise_for_status()

        posted_shout_out = ShoutOut.model_validate_json(r.text)
        posted_shout_outs.append(posted_shout_out)

    # query data
    recipients = all_recipients[:2]
    query = ShoutOutQuery(recipients=recipients)
    r = requests.put(f"{sos_url}/v1/shout-outs/query", json=query.model_dump())
    r.raise_for_status()
    shout_outs = ShoutOutList.model_validate_json(r.text)
    assert len(shout_outs.items) == len(recipients)
    assert shout_outs.item_count == len(recipients)
    queried_recipient_ids = [so.recipient_id for so in shout_outs.items]
    assert set(recipients) == set(queried_recipient_ids)
    for shout_out in shout_outs.items:
        message_ids = [m.id for m in shout_out.messages]
        assert len(message_ids) == num_messages_per_shout_out
        assert len(set(message_ids)) == num_messages_per_shout_out

    recipients = all_recipients
    query = ShoutOutQuery(recipients=recipients)
    params = {"skip": 1, "limit": 1}
    r = requests.put(f"{sos_url}/v1/shout-outs/query", json=query.model_dump(), params=params)
    r.raise_for_status()
    shout_outs = ShoutOutList.model_validate_json(r.text)
    assert len(shout_outs.items) == params["limit"]
    assert shout_outs.item_count == len(recipients)
    queried_recipient_ids = [so.recipient_id for so in shout_outs.items]
    assert shout_outs.items[0].recipient_id == recipients[1]
    for shout_out in shout_outs.items:
        message_ids = [m.id for m in shout_out.messages]
        assert len(message_ids) == num_messages_per_shout_out
        assert len(set(message_ids)) == num_messages_per_shout_out

    messages = []
    compare_shout_out_ids = []
    for posted_shout_out in posted_shout_outs[:2]:
        compare_shout_out_ids.append(str(posted_shout_out.id))
        for message in posted_shout_out.messages:
            messages.append(str(message.id))
    query = ShoutOutQuery(messages=messages)
    r = requests.put(f"{sos_url}/v1/shout-outs/query", json=query.model_dump())
    r.raise_for_status()
    shout_outs = ShoutOutList.model_validate_json(r.text)
    queried_shout_out_ids = [str(shout_out.id) for shout_out in shout_outs.items]
    assert set(compare_shout_out_ids) == set(queried_shout_out_ids)

    query = ShoutOutQuery(messages=["dummy"])
    r = requests.put(f"{sos_url}/v1/shout-outs/query", json=query.model_dump())
    assert r.status_code == 400

    query = ShoutOutQuery(messages=[str(uuid.uuid4())])
    r = requests.put(f"{sos_url}/v1/shout-outs/query", json=query.model_dump())
    r.raise_for_status()
    shout_outs = ShoutOutList.model_validate_json(r.text)
    assert shout_outs.item_count == 0
    assert len(shout_outs.items) == 0
