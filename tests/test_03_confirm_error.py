import requests

from shout_out_service.models import PostMessageSMTP, PostShoutOut, ShoutOut, ShoutOutStatus, Sink

from .singletons import sos_url


def test_confirm_shout_out():
    message_plain = PostMessageSMTP(
        sink=Sink.SMTP,
        to="To",
        subject="Subject",
        content="Content",
    )
    post_shout_out = PostShoutOut(
        recipient_id="dummy-id",
        timeout_interval=30,
        messages=[message_plain],
    )
    r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.status == ShoutOutStatus.UNCONFIRMED

    message_id = shout_out.messages[0].id
    confirmation = {"message_id": str(message_id)}
    r = requests.put(f"{sos_url}/v1/shout-outs/confirm", json=confirmation)
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out_compare.status == ShoutOutStatus.CONFIRMED


def test_error_shout_out():
    post_shout_out = PostShoutOut(
        recipient_id="dummy-id",
        timeout_interval=30,
        messages=[],
    )
    r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.status == ShoutOutStatus.UNCONFIRMED

    error = {"error_message": "something went wrong"}
    r = requests.put(f"{sos_url}/v1/shout-outs/{shout_out.id}/error", json=error)
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out_compare.status == ShoutOutStatus.ERROR
