import json
from time import sleep
from uuid import uuid4

import pika
import requests

from shout_out_service.models import PutDirectAMQP

from .singletons import settings, sos_url


def test_aqmp_shout_out():
    # Test assumes SOS_DAEMON_POLL_INTERVAL=15
    random_recipient = str(uuid4())
    routing_key = f"event-{random_recipient}"

    credentials = pika.PlainCredentials(settings.amqp_username, settings.amqp_password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost", credentials=credentials))
    channel = connection.channel()
    channel.queue_declare(queue=routing_key)

    put_direct_amqp = PutDirectAMQP(
        data={"hello": 42, "world": "!"},
        routing_key=routing_key,
    )

    r = requests.put(f"{sos_url}/v1/direct/amqp", json=put_direct_amqp.model_dump())
    r.raise_for_status()

    sleep(1)

    method_frame, header_frame, body = channel.basic_get(queue=routing_key, auto_ack=True)
    _, _ = method_frame, header_frame
    data = json.loads(body.decode("utf-8"))
    assert data["hello"] == 42
    assert data["world"] == "!"

    channel.close()
