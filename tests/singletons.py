from .settings import Settings

settings = Settings()
sos_url = settings.sos_url.rstrip("/")
mailhog_url = settings.mailhog_url.rstrip("/")
