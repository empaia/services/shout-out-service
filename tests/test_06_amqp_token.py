from time import sleep
from uuid import uuid4

import jwt
import pika
import requests

from shout_out_service.models import (
    MessageBodyAMQP,
    MessageStatus,
    PostMessageAMQP,
    PostShoutOut,
    ShoutOut,
    ShoutOutStatus,
    Sink,
)

from .singletons import settings, sos_url


def test_aqmp_shout_out():
    # Test assumes SOS_DAEMON_POLL_INTERVAL=15
    random_recipient = str(uuid4())
    routing_key = f"notify-{random_recipient}"

    credentials = pika.PlainCredentials(settings.amqp_username, settings.amqp_password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost", credentials=credentials))
    channel = connection.channel()
    channel.queue_declare(queue=routing_key)

    message = PostMessageAMQP(sink=Sink.AMQP, content="Hello Rabbit!", routing_key=routing_key)
    post_shout_out = PostShoutOut(
        recipient_id=random_recipient,
        timeout_interval=30,
        messages=[message],
    )
    r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.status == ShoutOutStatus.UNCONFIRMED
    assert len(shout_out.messages) == 1
    assert shout_out.messages[0].sink == message.sink
    assert shout_out.messages[0].content == message.content
    assert shout_out.messages[0].status == MessageStatus.READY.value
    assert shout_out.messages[0].routing_key == routing_key

    sleep(1)

    r = requests.get(f"{sos_url}/v1/shout-outs/{shout_out.id}")
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.messages[0].status == MessageStatus.SENT.value

    method_frame, header_frame, body = channel.basic_get(queue=routing_key, auto_ack=True)
    _, _ = method_frame, header_frame
    message_body = MessageBodyAMQP.model_validate_json(body.decode("utf-8"))
    assert message_body.content == message.content
    decoded_token = jwt.decode(message_body.shout_out_token, options={"verify_signature": False})
    assert decoded_token["sub"] == random_recipient
    assert decoded_token["message_id"] == str(shout_out.messages[0].id)
    assert decoded_token["sink"] == message.sink
    exp_diff = decoded_token["exp"] - shout_out.created_at
    assert abs(exp_diff - shout_out.timeout_interval) == 0

    channel.close()
