import requests

from shout_out_service.models import PostMessageSMTP, PostShoutOut, ShoutOut, Sink

from .singletons import sos_url


def test_post_get_shout_out():
    message_plain = PostMessageSMTP(
        sink=Sink.SMTP,
        to="To",
        subject="Subject",
        content="Token 1: {{SHOUT_OUT_TOKEN}}\r\nToken 2: {{SHOUT_OUT_TOKEN}}",
    )
    message_multipart = message_plain.model_copy()
    message_multipart.content_html = """
    <html><body>
    <h1>Content</h1>
    <p>Token 1: {{SHOUT_OUT_TOKEN}}<br/>Token 2: {{SHOUT_OUT_TOKEN}}</p>
    </body></html>
    """

    post_shout_out = PostShoutOut(
        recipient_id="dummy-id",
        timeout_interval=30,
        messages=[
            message_plain,
            message_multipart,
        ],
    )
    r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.recipient_id == post_shout_out.recipient_id
    assert shout_out.timeout_interval == post_shout_out.timeout_interval
    assert len(shout_out.messages) == len(post_shout_out.messages)
    assert shout_out.messages[0].to == post_shout_out.messages[0].to
    assert shout_out.messages[0].subject == post_shout_out.messages[0].subject
    assert shout_out.messages[0].content == post_shout_out.messages[0].content

    r = requests.get(f"{sos_url}/v1/shout-outs/{shout_out.id}")
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out.id == shout_out_compare.id
    assert shout_out.error_message == shout_out_compare.error_message
    assert shout_out.recipient_id == shout_out_compare.recipient_id
    assert len(shout_out.messages) == len(shout_out_compare.messages)
