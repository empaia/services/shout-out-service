from time import sleep
from uuid import uuid4

import requests

from shout_out_service.models import PostMessageAMQP, PostMessageSMTP, PostShoutOut, ShoutOut, ShoutOutStatus, Sink

from .singletons import sos_url


def test_timeout_anonymization_shout_out():
    # Test assumes SOS_DAEMON_POLL_INTERVAL=15
    random_subject = str(uuid4())

    message_plain = PostMessageSMTP(
        sink=Sink.SMTP,
        to="To",
        subject=random_subject,
        content="Content",
        content_html="""
        <html><body>
        <h1>Content</h1>
        <p>Paragraph</p>
        </body></html>
        """,
    )
    message_amqp = PostMessageAMQP(sink=Sink.AMQP, content="Hello Rabbit!")
    post_shout_out = PostShoutOut(
        recipient_id="dummy-id",
        timeout_interval=20,
        messages=[message_plain, message_amqp],
    )
    r = requests.post(f"{sos_url}/v1/shout-outs/", json=post_shout_out.model_dump())
    r.raise_for_status()
    shout_out = ShoutOut.model_validate_json(r.text)
    assert shout_out.status == ShoutOutStatus.UNCONFIRMED
    assert shout_out.messages[0].sink == post_shout_out.messages[0].sink
    assert shout_out.messages[1].sink == post_shout_out.messages[1].sink

    r = requests.get(f"{sos_url}/v1/shout-outs/{shout_out.id}")
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out_compare.messages[0].sink == post_shout_out.messages[0].sink
    assert shout_out_compare.messages[1].sink == post_shout_out.messages[1].sink

    sleep(10)
    r = requests.get(f"{sos_url}/v1/shout-outs/{shout_out.id}")
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out_compare.status == ShoutOutStatus.UNCONFIRMED

    sleep(30)
    r = requests.get(f"{sos_url}/v1/shout-outs/{shout_out.id}")
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out_compare.status == ShoutOutStatus.TIMEOUT
    assert len(shout_out_compare.messages) == len(shout_out.messages)
    assert shout_out_compare.messages[0].sink == post_shout_out.messages[0].sink
    assert shout_out_compare.messages[1].sink == post_shout_out.messages[1].sink
    message_compare = shout_out_compare.messages[0]
    assert message_compare.anonymized
    assert message_compare.to == "ANONYMIZED"
    assert message_compare.subject == "ANONYMIZED"
    assert message_compare.content == "ANONYMIZED"
    assert message_compare.content_html == "ANONYMIZED"
    message_compare = shout_out_compare.messages[1]
    assert message_compare.anonymized
    assert message_compare.content == "ANONYMIZED"

    sleep(15)
    r = requests.get(f"{sos_url}/v1/shout-outs/{shout_out.id}")
    r.raise_for_status()
    shout_out_compare = ShoutOut.model_validate_json(r.text)
    assert shout_out_compare.anonymized
