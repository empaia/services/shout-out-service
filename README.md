# Shout Out Service

The Shout Out Service (SOS) sends Shout Outs and tracks the confirmation status.

## Usage Scenarios

A Shout Out is a data structure, that wraps multiple messages to be sent to a user. Each message in a single Shout Out has similar content, but should reach the user in different ways. Ideally we want a user to confirm having received a Shout Out, for example by clicking on a link in a email or clicking on a notification in a web app. If the Shout Out is not being confirmed by the user, it will change into a timeout status after a defined interval.

The SOS does not just work on its own, because other services in the service infrastructure use it as a communication backend. Therefore the SOS can be hidden in a private network (auth may be disabled) or can be exposed in public network (service-to-service auth must be enabled). The SOS requires an SMTP service (e.g. mailhog) and/or an AMQP service (e.g. RabbitMQ) configured as a message sink. In addition, some other service is required to consume the AMQP messages and send them to clients (e.g. via web-sockets or server-sent-events).

Currently the SOS supports two sink types to send messages: SMTP and AMQP. A Shout Out can be sent through multiple sinks at the same time. For example, if a Shout Out is very important, send the messages through both sinks, such that the user receives an email via his primary email address (SMTP), an email via his secondary email address (SMTP), a notification in a web app (AMQP) and a notification in a mobile app (AMQP). For this purpose, different routing keys can be defined for AMQP messages. For less important Shout Outs, a single message delivered to a web app could be sufficient.

Another use case for SOS are user registration emails. In this case, create a Shout Out with a single SMTP message. The SMTP message should contain a link to a web app, that processes the confirmation via a different (auth) service. Optionally, SOS can automatically embed a JSON Web Token (JWT) by specifying the variable `{{SHOUT_OUT_TOKEN}}` inside the SMTP message `content` or `content_html` string. For example the message could contain a link like `https://example.com/register/{{SHOUT_OUT_TOKEN}}`. The service at `example.com`, can validate the JWT by retrieving the public key of the SOS via a dedicated HTTP route. The JWT contains the `sink` type, the `message_id` and the `recipient_id` as `sub` and therefore provides everything required to verify, that the user has received the email via the SOS.

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone shout-out-service

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd shout-out-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Or start `shout_out_service` with uvicorn and only a development database with docker-compose.

```bash
docker-compose up -d sos-db
sosctl migrate-db
uvicorn --host=0.0.0.0 --port=8000 --reload shout_out_service.app:app
# in a separate shell
sosd
```

Access the interactive OpenAPI specification in a browser:

* http://localhost:8000/docs
* http://localhost:8000/v1/docs

### Stop and Remove

```bash
docker-compose down -v
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black shout_out_service tests tests_auth
pycodestyle shout_out_service tests tests_auth
pylint shout_out_service tests tests_auth
pytest tests --maxfail=1  # requires service running
```
