#!/usr/bin/env bash

echo "DOWN:"
docker-compose down -v --remove-orphans -t 0
echo "UP:"
docker-compose up --build -d
echo "LOGS:"
docker-compose logs -f
