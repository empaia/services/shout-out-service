# 0.3.27

* updated aiosmtplib to 3.0.1
* removed SMTP source address parameter

# 0.3.18

* using FastAPI lifespan instead of startup event

# 0.3.16

* renovate

# 0.3.15

* renovate

# 0.3.13

* renovate

# 0.3.12

* renovate

# 0.3.11

* renovate

# 0.3.10

* renovate

# 0.3.9

* renovate

# 0.3.8

* renovate

# 0.3.7

* renovate

# 0.3.6

* renovate

# 0.3.5

* renovate

# 0.3.4

* renovate

# 0.3.3

* fix for using sosd with pydantic v2 and local maildev

# 0.3.2

* renovate

# 0.3.1

* bugfix for empty `rewrite_url_in_wellknown` env variable

# 0.3.0

* migration to pydantic v2

# 0.2.47

* renovate

# 0.2.46

* renovate

# 0.2.45

* renovate

# 0.2.44

* renovate

# 0.2.43

* renovate

# 0.2.42

* renovate

# 0.2.41

* renovate

# 0.2.40

* renovate

# 0.2.39

* renovate

# 0.2.38

* renovate

# 0.2.37

* renovate

# 0.2.36

* renovate

# 0.2.35

* renovate

# 0.2.34

* renovate

# 0.2.33

* renovate

# 0.2.32

* renovate

# 0.2.31

* renovate

# 0.2.30

* renovate

# 0.2.29

* renovate

# 0.2.28

* renovate

# 0.2.27

* renovate

# 0.2.26

* added direct AMQP route

# 0.2.25

* renovate

# 0.2.24

* renovate

# 0.2.23

* renovate

# 0.2.22

* renovate

# 0.2.21

* renovate

# 0.2.20

* renovate

# 0.2.19

* renovate

# 0.2.18

* renovate

# 0.2.17

* renovate

# 0.2.16

* renovate

# 0.2.15

* renovate

# 0.2.14

* added daemon ticks to trigger polling when data has been updated through web requests

# 0.2.13

* renovate

# 0.2.12

* renovate

# 0.2.11

* renovate

# 0.2.10

* renovate

# 0.2.9

* renovate

# 0.2.8

* renovate

# 0.2.7

* renovate

# 0.2.6

* renovate

# 0.2.5

* renovate

# 0.2.4

* renovate

# 0.2.3

* renovate

# 0.2.2

* renovate

# 0.2.1

* renovate

# 0.2.0

* AMQP support
* confirm route changed
  * shout-out-id not used anymore, only message-id

# 0.1.4

* renovate

# 0.1.3

* renovate

# 0.1.2

* first functional release
* SMTP support

# 0.1.1

* updated ci
* release not functional

# 0.1.0

* init
* release not functional
